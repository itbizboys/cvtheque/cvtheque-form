import { FileValidator } from 'ngx-material-file-input';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { UploaderService } from './uploader.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  profileForm : FormGroup;
  // username = new FormControl('', [Validators.required]);
  // email = new FormControl('', [Validators.required, Validators.email]);
  // city = new FormControl('', [Validators.required]);
  // profile = new FormControl('', [Validators.required]);
  // experience = new FormControl('', [Validators.required]);
  // languages = new FormControl('', [Validators.required]);
  // fileControl: FormControl;

  color: ThemePalette = 'primary';
  disabled: boolean = false;
  multiple: boolean = false;
  accept: string;
  showSpinner : boolean = true;
  response : any;
  
  title = 'cvtheque-form';
  public files;
  maxSize = 5;
  
  constructor(private formBuilder: FormBuilder, private service:UploaderService, private _snackBar: MatSnackBar) {
    
    // this.fileControl = new FormControl(this.files, [
    //   Validators.required,
    //   MaxSizeValidator(this.maxSize * 1024)
    // ])
   }

  ngOnInit() {
    this.showSpinner = false;
    this.profileForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      city: ['', Validators.required],
      profile: ['', Validators.required],
      experience: ['', Validators.required],
      languages: ['', Validators.required],
      fileControl: [undefined, [Validators.required]],
    });

    
    // this.fileControl.valueChanges.subscribe((files: any) => {
    //   if (!Array.isArray(files)) {
    //     this.files = [files];
    //   } else {
    //     this.files = files;
    //   }
    // })
  }

  get data() { return this.profileForm.controls; }

  onSubmit() {
    this.showSpinner = true;
    if (this.profileForm.invalid) {
      this.showSpinner = false;
      return;
    }else{
      let body = new FormData();
      body.append('fullname', this.data.username.value);
      body.append('email', this.data.email.value);
      body.append('profile', this.data.profile.value);
      body.append('city', this.data.city.value);
      body.append('experience', this.data.experience.value);
      body.append('languages', this.data.languages.value);
      body.append('file', this.data.fileControl.value)
      this.service.candidateUploader(body).subscribe(
        res => {
          this.response = res;
          if(this.response == "Data Submitted Successfully"){
            this._snackBar.open(this.response, '', {
              duration: 2000,
            });
          } else{
            this._snackBar.open("Please Try Again", '', {
              duration: 2000,
            });
          }
          
        },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            //A client-side or network error occurred.				 
            console.log('An error occurred:', err.error.message);
          } else {
            //Backend returns unsuccessful response codes such as 404, 500 etc.				 
            console.log('Backend returned status code: ', err.status);
            console.log('Response body:', err.error);
          }
        }
      )
      this.showSpinner = false;
    }
  }

  upload() {

    // if(this.username.invalid){
    //   warn.log();
      
    // }
    // console.log(this.files[0])
    let body = new FormData();

    // body.append('fullname', this.username.value);
    // body.append('email', this.email.value);
    // body.append('profile', this.profile.value);
    // body.append('city', this.city.value);
    // body.append('experience', this.experience.value);
    // body.append('languages', this.languages.value);
    body.append('file', this.files[0])

    this.service.candidateUploader(body).subscribe(
      res => {
        let response = res.body;
        console.log(response)
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          //A client-side or network error occurred.				 
          console.log('An error occurred:', err.error.message);
        } else {
          //Backend returns unsuccessful response codes such as 404, 500 etc.				 
          console.log('Backend returned status code: ', err.status);
          console.log('Response body:', err.error);
        }
      }
    )
    // this.service.candidateUploader("John Wick", "john@gmail.com", "Casablanca", "Manager", "expert", "English, French, Others")
    
  }

  // getEmailErrorMessage() {
  //   if (this.email.hasError('required')) {
  //     return 'You must enter a value';
  //   }
  //   return this.email.hasError('email') ? 'Not a valid email' : '';
  // }

  // getErrorMessage() {
  //   if (this.username.hasError('required')) {
  //     return 'You must enter a value';
  //   }

  //   if (this.city.hasError('required')) {
  //     return 'You must choose a value';
  //   }

  //   if (this.profile.hasError('required')) {
  //     return 'You must choose a value';
  //   }

  //   if (this.experience.hasError('required')) {
  //     return 'Field Experience is Required';
  //   }
  // }

  // getFileErrorMessage() {
  //   if (this.fileControl.hasError('required')) {
  //     return 'You must upload your Resume';
  //   }
  //   return this.fileControl.hasError('fileControl') ? 'Not a valid File' : '';
  // }

}
