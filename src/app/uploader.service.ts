import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs/internal/observable/throwError';
import {catchError} from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class UploaderService {

  // baseURL: string = "http://127.0.0.1:5000/";
  baseURL: string = "http://5.189.157.89:5000/";

  constructor(private http:HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  /**
   * candidateUploader
   */
  // public candidateUploader(fullname: string, email: string, city: string, profile: string, experience: string,languages: string) {
    
  public candidateUploader(body: any): Observable<HttpResponse<any>>{
    // const headers = {"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "access-control-allow-origin, access-control-allow-headers"};
    // let httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/json',
    //     "Access-Control-Allow-Origin": "*",
    //     "Access-Control-Allow-Headers": "access-control-allow-origin, access-control-allow-headers"
    //   })
    // };
    // let body = {"fullname": fullname, "email": email, "city": city, "profile": profile, "experience": experience, "languages": languages, "file": file};
    
    return this.http.post<any>( this.baseURL + 'submitreq', body);
    // .subscribe(data => {
    //     // this.postId = data.id;
    // });
  }
}
